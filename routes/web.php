<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    return view('welcome');
});

//登陆路由
Route::get('admins/account/login', 'Admins\Account@login')->name('login');
Route::post('admins/account/dologin', 'Admins\Account@dologin')->middleware('throttle:5,1')->name('dologin');

Route::namespace('Admins')->middleware(['auth','rights'])->group(function (){
    Route::middleware('auth')->get('/admins/home/index', 'Home@index');
    Route::get('/admins/home/ajax_get_left_menu', 'Home@ajax_get_left_menu');
    Route::get('/admins/home/welcome', 'Home@welcome');
    Route::get('/admins/admins/index', 'Admins@index');
    Route::get('/admins/admins/add', 'Admins@add')->name('addAdmin');
    Route::post('admins/admins/save','Admins@save');
    //菜单管理
    Route::get('admins/menus/index','Menus@index');
    Route::get('admins/menus/add','Menus@add');
    Route::post('admins/menus/save','Menus@save');
    Route::get('admins/menus/delete','Groups@delete');

});

Route::group(['prefix' => 'admins'], function()
{
    Route::get('users', function(){
        return 'Matches The "/admin/users" URL';
    });
});
