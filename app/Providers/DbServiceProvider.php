<?php


namespace App\Providers;


use Illuminate\Database\Query\Builder;
use Illuminate\Support\ServiceProvider;


class DbServiceProvider extends ServiceProvider
{

    public function boot()
    {
        Builder::macro('item',function(){
            $item = $this->first();
            return $item ? (array)$item : false;
        });

        Builder::macro('lists',function(){
            $result = $this->get()->all();
            $lists = [];
            foreach($result as $item){
                $lists[] = (array)$item;
            }
            return $lists;
        });

        Builder::macro('cates',function($index){
            $result = $this->get()->all();
            $lists = [];
            foreach($result as $item){
                $lists[$item->$index] = (array)$item;
            }
            return $lists;
        });

        // 扩展pages方法
        Builder::macro('pages',function($perPage = null, $columns = ['*'], $pageName = 'page', $page = null){
            $result = $this->paginate($perPage,$columns,$pageName,$page);
            $items = $result->items();
            $result->lists = [];
            foreach ($items as $val) {
                $result->lists[] = (array)$val;
            }
            return $result;
        });
     }

}
