<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use DB;
class RightsValidate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        $role = DB::table('admin_group')->where('gid', $user->group_id)->first();
        $role->rights = json_decode($role->rights, true);
        if (empty($role)) {
            return redirect('home/dashboard');
        }
        $actions = $request->route()->getActionName();
        $actions_arr = explode('@', $actions);
        $controllers = explode('\\', $actions_arr[0]);
        $controller = $controllers[count($controllers) - 1];
        $action = $actions_arr[1];
        $menu = DB::table('admin_menu')->where([
            'controller' => $controller,
            'action' => $action,
            ])->first();

        if (empty($menu)) {
          return  $this->norights($request);
        }

        if (!in_array($menu->mid, $role->rights)) {
           return $this->norights($request);
        }
        $request->rights = $role->rights;
        return $next($request);
    }

    public function norights($request)
    {
        if ($request->ajax()) {
            return response()->json(['code' => 1,'msg' => '菜单不存在']);
        }
        return redirect(route('login'));
    }
}
