<?php

namespace App\Http\Controllers\Admins;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;

class Home extends Controller
{
    //
    public function index(Request $request)
    {

        return view('admins.home.index');
    }

    public function welcome(Request $req){
        return view('admins.home.welcome');
    }


    public function ajax_get_left_menu(Request $request)
    {
        $role = DB::table('admin_group')->where('gid',Auth::user()->group_id)->first();
        $menus = false;

        $menus = false;
        if($role->rights){

            $where = 'mid in('.implode(',', $request->rights).') and ishidden=0 and status=0';
            $menus = DB::table('admin_menu')->whereRaw($where)->orderBy('ord','asc')->orderBy('mid','asc')->get()->all();
        }


        if($menus){
            $temp = [];
            foreach ($menus as $key => $value) {
                $value = (array)$value;
                $menus[$key] = $value;

                $value['controller'] = strtolower($value['controller']);

                $value['action'] = strtolower($value['action']);

                $temp[$value['mid']] = $value;
              //  $temp[$key]['url'] = '/admins/'.$value['controller'].'/'.$value['action'];
            }
            $menus = $temp;

            $menus = $this->_formateMenus($menus);

        }

        echo json_encode(array('code'=>0,'data'=>$menus));

    }

    public function recursive_make_tree($list, $pk = 'Fid', $pid = 'pid', $child = '_child', $root = 0)
    {
        $tree = [];
        foreach ($list as $key => $val) {
            if ($val[$pid] == $root) {
                //获取当前$pid所有子类
                unset($list[$key]);
                if (!empty($list)) {
                    $child = $this->recursive_make_tree($list, $pk, $pid, $child, $val[$pk]);
                    if (!empty($child)) {
                        $val['_child'] = $child;
                    }
                }
                $tree[] = $val;
            }
        }
        return $tree;
    }

    private function _formateMenus($items){
        $tree = [];
        foreach ($items as $item){
            if (isset($items[$item['pid']])){
                $items[$item['pid']]['children'][] = &$items[$item['mid']];
            }else{
                $tree[] = &$items[$item['mid']];
            }
        }
        return $tree;
    }
}
