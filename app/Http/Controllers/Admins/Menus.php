<?php

namespace App\Http\Controllers\Admins;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class Menus extends Controller
{
     public function index(Request $request)
     {
         $data['pid'] = (int)$request->pid;
         $data['data'] = DB::table('admin_menu')->where(array('pid'=>$data['pid']))->orderBy('ord','asc')->orderBy('mid','asc')->lists();
         // 返回上一级菜单
         $data['backId'] = 0;
         if($data['pid'] > 0){
             $parent = DB::table('admin_menu')->where(array('mid'=>$data['pid']))->item();
             $data['backId'] = $parent['pid'];
         }
         return view('admins/menus/index',$data);
     }
}
