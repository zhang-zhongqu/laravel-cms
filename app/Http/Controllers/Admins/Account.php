<?php


namespace App\Http\Controllers\Admins;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use DB;


class Account extends  Controller
{
    public function login()
    {

        return view('admins/account/login');
    }

    public function dologin(Request $request)
    {
        $user_name = $request->username;
        $pwd = $request->password;
        $verify_code = $request->verifycode;

        if (empty($user_name)) {
            return response()->json(['code' => 1, 'msg' => '用户名不能为空']);

        }

        if (empty($pwd)) {
            return response()->json(['code' => 1, 'msg' => '密码不能为空']);

        }

        if (empty($verify_code)) {
            return response()->json(['code' => 1, 'msg' => '验证码不能为空']);

        }

        $res = Auth::attempt(['username' => $user_name, 'password' => $pwd,'status' => 0]);

        if ($res) {
            DB::table('admin')->where('id', Auth::id())->update(['login_lastip' => $request->getClientIp(), 'login_lasttime' => time()]);
            return response()->json(['code' => 0, 'msg' => '登陆成功']);
        }
        return response()->json(['code' => 1, 'msg' => '登陆失败']);


    }
}
