<?php

namespace App\Http\Controllers\Admins;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class Admins extends Controller
{
    //
    public function index()
    {
        $data['lists'] = DB::table('admin')->lists();

        $groups = DB::table('admin_group')->pluck('title','gid')->toArray();

        foreach ($data['lists'] as $key => $val) {

            $data['lists'][$key]['group_title'] = $groups[$val['group_id']];
        }

        return view('admins.admin.index', $data);

    }

    public function add(Request $request)
    {
        $admin_id = $request->admin_id;
        $data['admin'] = DB::table('admin')->where('id', $admin_id)->item();
        $data['groups'] = DB::table('admin_group')->lists();

        return view('admins.admin.add', $data);
    }

    public function save(Request $request)
    {

        $data = [];
        $id = (int)$request->id;
        $username = trim($request->user_name);
        $password = trim($request->pwd);
        $data['group_id'] = (int)$request->group_id;
        $data['real_name'] = trim($request->real_name);
        $data['mobile'] = trim($request->mobile);
        $data['status']= (int)$request->status;


        if(empty($id) && $password==''){
           return  response()->json(['code'=>1,'msg'=>'密码不能为空']);
        }
        if(empty($id) && !$username){
            return  response()->json(['code'=>1,'msg'=>'用户名不能为空']);
        }
        if(empty($data['group_id'])){
            return  response()->json(['code'=>1,'msg'=>'未选择管理员角色']);
        }
        if(empty($data['real_name'])){
            return  response()->json(['code'=>1,'msg'=>'真实姓名不能为空']);
        }

        if($id){
            if($password){

                $data['password'] = password_hash($password,PASSWORD_DEFAULT);
            }
            $data['edit_time'] = time();

            $res = DB::table('admin')->where('id',$id)->update($data);
            $descs = '编辑管理员：《'.$data['real_name'].'》,ID：'.$id;
        }else{
            $data['add_time'] = time();
            $data['username'] = $username;
            $data['password'] = password_hash($password,PASSWORD_DEFAULT);

            $res = DB::table('admin')->insertGetId($data);

            $descs = '添加管理员：《'.$data['real_name'].'》,ID：'.$res;
        }

        if ($res) {
            return  response()->json(['code'=>0,'msg'=>'保存成功']);
        }
        //添加操作日志
     //   $this->oplog($request->_admin['id'],$descs);
     //   exit(json_encode(array('code'=>0,'msg'=>'保存成功')));
    }
}
