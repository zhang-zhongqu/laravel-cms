<meta charset="utf-8">
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="format-detection" content="telephone=no">
<link rel="stylesheet" href="/static/css/style.css"  media="all">
<script type="text/javascript" src="/static/js/jquery3.4.1.js"></script>
<script type="text/javascript" src="/static/layer/layer.js"></script>
<script type="text/javascript" src="/static/js/phpcn.js"></script>
<script type="text/javascript" src="/static/js/global.js"></script>
<script src="{{ asset('static/js/vue.min.js') }}"></script>
<script src="https://www.layui.com/layuiadmin/std/dist/layuiadmin/layui/layui.js"></script>
<link rel="stylesheet" href="https://www.layui.com/layuiadmin/std/dist/layuiadmin/layui/css/layui.css" media="all">
