<!DOCTYPE html>
<html>
<head>
    <title>添加、修改管理员</title>
    @include('admins.public.init')
</head>
<body style="padding: 10px;">
<div class="phpcn-form phpcn-bg-fff phpcn-p-10" id="app">
    {{csrf_field()}}
    <input type="hidden" id="id" value="">
    <div class="phpcn-form-item phpcn-bg-fff ">
        <label class="phpcn-form-lable">用户名：</label>
        <div class="phpcn-input-inline">
            <input type="text" class="phpcn-input" id="username" v-model.trim="user.user_name" {{$admin['id']>0?'readonly':''}}>
            <input type="hidden" id="id" name="id" v-model="user.id">
        </div>
    </div>
    <div class="phpcn-form-item phpcn-bg-fff ">
        <label class="phpcn-form-lable">角色：</label>
        <div class="phpcn-input-inline layui-form" >
            <select name="modules" id="text" lay-verify="required" lay-search=""  lay-filter="category">
                <option value=0>请选择</option>
                @foreach($groups as $group)
                    <option :value="{{$group['gid']}}"  {{ $group['gid'] == $admin['group_id'] ? 'selected=selected':''}} >{{$group['title']}}</option>
                @endforeach
            </select>

        </div>
    </div>
    <div class="phpcn-form-item phpcn-bg-fff ">
        <label class="phpcn-form-lable">密码：</label>
        <div class="phpcn-input-inline">
            <input type="password" class="phpcn-input" placeholder="不修改则无需填写" id="pwd" v-model.trim="user.pwd">
        </div>
    </div>
    <div class="phpcn-form-item phpcn-bg-fff ">
        <label class="phpcn-form-lable">姓名：</label>
        <div class="phpcn-input-inline">
            <input type="text" class="phpcn-input" id="real_name"  v-model.trim="user.real_name">
        </div>
    </div>

    <div class="phpcn-form-item phpcn-bg-fff ">
        <label class="phpcn-form-lable">手机：</label>
        <div class="phpcn-input-inline">
            <input type="text" class="phpcn-input" id="real_name"  v-model.trim="user.mobile">
        </div>
    </div>

    <div class="phpcn-form-item phpcn-bg-fff ">
        <label class="phpcn-form-lable">设置：</label>
        <div class="phpcn-input-inline phpcn-form-checkbox2" >

            <input type="checkbox" id="status"  title="禁用"  {{ $admin['status'] == 1 ? 'checked':'' }}  >
        </div>
    </div>

    <div class="phpcn-form-item phpcn-bg-fff">
        <div class="phpcn-tx-c">
            <button class="phpcn-button" type='button' @click="admin_save();">保存</button>
            <button class="phpcn-button phpcn-bg-black"  type='button' onclick="model_cancel();">取消</button>
        </div>
    </div>
</div>
<script>
    var form;
    $(function(){
        layui.use('form', function(){
            form = layui.form;
            form.render();
        });
    });
    var app = new Vue({
        el: '#app',
        data: {
            user:{
                user_name: '',
                group_id: '',
                pwd: '',
                real_name: '',
                mobile: '',
                id: '',
                _token :  document.querySelector('input[name=_token]').value,
                status:  $('#status').is(':checked') ? 1 : 0

            },
        },
        mounted() {
            this.user.user_name =  "{{ $admin['username'] }}";
            this.user.group_id =  "{{ $admin['group_id'] }}";
            this.user.real_name =  "{{ $admin['real_name'] }}";
            this.user.mobile =  "{{ $admin['mobile'] }}";
            this.user.id =  "{{ $admin['id'] }}";

        },
        methods: {
            admin_save(){
                this.user.status = $('#status').is(':checked') ? 1 : 0;

                this.user.group_id = $('.layui-this').attr('lay-value');
                if(this.user.user_name == ''){
                    layer.msg('请填写用户名',{'icon':2,'offset':'t','anim':6});
                    return;
                }
                if(this.user.group_id == ''){
                    layer.msg('请选择角色',{'icon':2,'offset':'t','anim':6});
                    return;
                }
                if(this.user.id == '' && this.user.pwd == ''){
                    layer.msg('请输入密码',{'icon':2,'offset':'t','anim':6});
                    return;
                }
                /* if(this.user.real_name==''){
                   layer.msg('请填写真实姓名',{'icon':2,'offset':'t','anim':6});
                   return;
               }*/
                $.post('/admins/admins/save',this.user,function(res){
                    if(res.code>0){
                        layer.msg(res.msg,{'icon':2,'offset':'t','anim':6});
                    }else{
                        layer.msg(res.msg,{time:1000,'icon':1},function(){
                            parent.window.location.reload();
                        });

                    }
                },'json');
            }
        }
    })

</script>
</body>
</html>
