<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>管理员列表</title>
    @include('admins.public.init')
</head>
<body>
<div id="app" class="phpcn-pd-10 phpcn-bg-fff">
    {{csrf_field()}}
    <button class="phpcn-button" @click="admin_add">添加</button>
    <table class="phpcn-table">
        <thead>
        <tr>
            <th>ID</th>
            <th>用户名</th>
            <th>真实姓名</th>
            <th>分组</th>
            <th>最后登录时间</th>
            <th>状态</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody>
        @foreach($lists as $item)
            <tr>
                <td>{{ $item['id'] }}</td>
                <td>{{ $item['username'] }}</td>
                <td>{{ $item['real_name'] }}</td>
                <td>{{ $item['group_title'] }}</td>
                <td>{{ $item['login_lasttime'] ? date('Y-m-d H:i:s',$item['login_lasttime']) : ''}}</td>
                <td>{!!$item['status'] == 0 ? '正常' : '<span style="color:#FF5722">禁用</span>'!!}</td>
                <td>
                    <button class="phpcn-button phpcn-bg-black phpcn-button-edit" @click="admin_add({{$item['id']}})" >修改</button>
                    <button class="phpcn-button phpcn-bg-red phpcn-button-edit" @click="admin_del({{$item['id']}})">删除</button>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

</div>

<script type="text/javascript">
    var app = new Vue({
        el:'#app',
        data:{
            urls:'',
            items:[]
        },
        methods: {
            admin_add(admin_id) {

                layer.open({
                    type: 2,
                    title: admin_id > 0 ? '添加' : '修改' + '管理员',
                    shadeClose: true,
                    shade: 0.8,
                    area: ['100%', '100%'],
                    content: "{{route('addAdmin')}}" + '?admin_id=' + admin_id,
                });
            }
        },
    });

</script>
</body>

</html>
