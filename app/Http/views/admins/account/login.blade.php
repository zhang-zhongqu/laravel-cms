
<!DOCTYPE html>
<html>
<head>
    <title>登录</title>
    <link rel="stylesheet" href="/static/css/style.css"  media="all">
    <script src="/static/js/jquery3.4.1.js"></script>
    <script type="text/javascript" src="/static/layer/layer.js"></script>
</head>
<body style="background: #1E9FFF;">
<div style="position: absolute;left: 50%;top:50%;width: 480px;margin-left: -240px;margin-top:-200px;">
    <div style="background-color: #ffffff;padding: 20px;border-radius: 4px;box-shadow: 5px 5px 20px #444444;">
        <div class="phpcn-form">
            {{csrf_field()}}
            <div class="phpcn-form-item" style="color: gray;">
                <h2>ZZQ 后台管理系统</h2>
            </div>
            <hr>
            <div class="phpcn-form-item">
                <label class="phpcn-form-lable">用户名</label>
                <div class="phpcn-input-block">
                    <input type="text" class="phpcn-input" id="username">
                </div>
            </div>
            <div class="phpcn-form-item">
                <label class="phpcn-form-lable">密&nbsp;&nbsp;&nbsp;&nbsp;码</label>
                <div class="phpcn-input-block">
                    <input type="password" class="phpcn-input" id="password">
                </div>
            </div>
            <div class="phpcn-form-item">
                <label class="phpcn-form-lable">验证码</label>
                <div class="phpcn-input-block">
                    <input type="text" class="phpcn-input" id="verifycode">
                </div>
                <img src="{{captcha_src()}}"   id="captcha" style="border: 1px solid #cdcdcd;cursor:pointer;"  onclick="reload_captcha()">

            </div>
            @if($errors->has('captcha'))
                <div class="col-md-12">
                    <p class="text-danger text-left"><strong>{{$errors->first('captcha')}}</strong></p>
                </div>
            @endif


            <div class="phpcn-form-item">
                <div class="phpcn-input-block">
                    <button class="phpcn-button" onclick="dologin()">登录</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $('#username').focus();

    // 回车登录
    $('input').keydown(function(e){
        if(e.keyCode==13){
            dologin();
        }
    });

    function reload_captcha(){
        $('#captcha').attr('src','{{captcha_src()}}'+Math.random());
    }

    function dologin(){
        var username = $.trim($('#username').val());
        var pwd = $.trim($('#password').val());
        var verifycode = $.trim($('#verifycode').val());
        var token = $('input[name="_token"]').val();
        if(username == ''){
            layer.alert('请填写用户名',{icon:2});
            return;
        }
        if(pwd == ''){
            layer.alert('请填写密码',{icon:2});
            return;
        }
        if(verifycode==''){
            layer.alert('请填写验证码',{icon:2});
            return;
        }
        $.ajax({
            type: 'post',
            url:"{{ route('dologin') }}",
            data: {_token:token,'username':username,'password':pwd,'verifycode':verifycode},
            success: function(res){
                if(res.code>0){
                    layer.msg(res.msg,{icon:2});
                    reload_captcha();
                }else{
                    layer.msg(res.msg);
                    setTimeout(function(){
                        window.location.href = '/admins/home/index';
                    },1000);
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {

                layer.msg('系统异常',{icon:2});
            },
            dataType:'json'
        });
    }
</script>
</body>
</html>
