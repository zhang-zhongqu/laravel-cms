<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>zzq后台管理系统</title>
    <link rel="stylesheet" href="{{ asset('static/css/style.css') }}"  media="all">
    <script type="text/javascript" src="{{ asset('static/js/jquery3.4.1.js') }}"></script>
    <script type="text/javascript" src="{{ asset('static/layer/layer.js') }}"></script>
    <script type="text/javascript" src="{{ asset('static/js/phpcn.js') }}"></script>
    <script type="text/javascript" src="{{ asset('static/js/global.js') }}"></script>
    <script src="{{ asset('static/js/vue.min.js') }}"></script>

</head>
<body>
        <div class="top">
            <div class="phpcn-l area">欢迎使用zzq后台网站管理系统</div>
            <div class="user phpcn-r phpcn-mr-20">
                <span class="">快捷管理中心</span>
                <span id='admin-select' class='bgcolor'>{{ Auth::user()->username }}
                    <dl class='user-pull-down'>
                        <a href="javascript:;">修改密码</a>
                        <!-- <a href="">登陆历史</a> -->
                        <a href="javascript:;" onclick="logout()">退出登录</a>
                    </dl>
                </span>
            </div>
        </div>

        <div class="container phpcn-clear">
            <!--左侧导航-->
            <div class="phpcn-tree phpcn-l ">
                <div class="logo">
                    <img src="/static/images/logo.jpg" width='100%'>
                </div>
                <ul class="tree phpcn-clear" id="phpcn_nav_side">
                    <li v-for="(item,index,key) in items">
                        <h2>
                            <span class="phpcn-icon" v-bind:class="item.icon"></span><label v-text="item.title"></label><i class="phpcn-r phpcn-icon phpcn-icon-down "></i>
                        </h2>
                        <template v-if="item.children!=undefined && item.children.length !== 0">
                            <dl>
                                <a href="javascript:;" v-for="ch in item.children" v-bind:data="ch.url" onclick="menu_fire(this)" v-bind:controller="ch.controller" v-bind:action="ch.action" v-text="ch.title" ></a>
                            </dl>
                        </template>
                    </li>
                </ul>
            </div>
            <!--左侧导航结束-->
            <div class="phpcn-content phpcn-r">
                <div class="header">
                    <div class="h-left phpcn-clear">
                        <ul class="phpcn-l-li phpcn-clear">
                            <li><i class='phpcn-icon phpcn-icon-liebiao'></i></li>
                            <li><i class='phpcn-icon phpcn-icon-shouyeshouye'></i></li>
                            <li class='phpcn-ps-r'>
                                <input type="" name=""> <i class='phpcn-ps-a phpcn-icon phpcn-icon-sousuo2'></i>
                            </li>
                        </ul>
                    </div>
                    <div class='phpcn-tab-title phpcn-mt-10 '>
                        <ul class="phpcn-l-li phpcn-clear">
                            <!--<span class='arrow'>
                                <i></i>
                                <dd> <a class="phpcn-tx-c phpcn-icon phpcn-icon-doubleleft"></a></dd>
                            </span>-->
                            <li class="on">
                                <i></i>
                                <dd>主页 <!--<a class="phpcn-icon phpcn-icon-guanbi"></a>--></dd>
                            </li>
                            <!--<span class='arrow'>
                                <i></i>
                                <dd href=""><a class="phpcn-icon phpcn-icon-doubleright"></a></dd>
                            </span>-->
                        </ul>
                    </div>
                </div>
                <div class="phpcn-row phpcn-clear ">
                    <iframe id="main_frame" src="/admins/home/welcome" frameborder="0" scrolling="auto" onload="resetHeight(this)"></iframe>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            var vm_menus = new Vue({
                el:'#phpcn_nav_side',
                data:{
                    urls:'',
                    items:[]
                }
            });
            init();
            load_menu();
        </script>
</body>
</html>
