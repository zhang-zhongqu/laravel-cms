<!DOCTYPE html>
<html>
<head>
    <title>菜单列表</title>
    @include('admins.public.init')
</head>
<body>
<div class="phpcn-form phpcn-pd-10 phpcn-bg-fff">
    @if($pid)
        <button class="phpcn-button phpcn-button-xs" style="float: right;margin:5px 0px;" onclick="menus_back({{$backId}});return false;">返回上一级</button>
    @endif
    <input type="hidden" name="pid" value="<?=$pid?>">
    <button type="button" class="phpcn-button phpcn-bg-black phpcn-button-xs phpcn-r phpcn-mb-5" onclick="menus_add();">添加</button>
    <table class="phpcn-table">
        <thead>
        <tr>
            <th>ID</th>
            <th>排序</th>
            <th>菜单名称</th>
            <th>Controller</th>
            <th>action</th>
            <th>是否隐藏</th>
            <th>是否禁用</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $item)
            <tr>
                <td><?=$item['mid']?></td>
                <td style="width: 60px;">{{$item['ord']}}</td>
                <td>{{$item['title']}}</td>
                <td>{{$item['controller']}}</td>
                <td>{{$item['action']}}</td>
                <td>{{$item['ishidden']?'隐藏':'正常'}}</td>
                <td>{{$item['status']?'是':'否'}}</td>
                <td>
                    <button type="button" class="phpcn-button phpcn-button-xs" onclick="menus_child({{$item['mid']}});">子菜单</button>
                    <button type="button" class="phpcn-button phpcn-bg-black phpcn-button-xs" onclick="menus_add({{$item['mid']}});">修改</button>
                    <button type="button" class="phpcn-button phpcn-bg-red phpcn-button-xs" onclick="menus_del({{$item['mid']}});">删除</button>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
</body>
</html>
